package com.mastertech.controletarefas.web;

import com.mastertech.controletarefas.persistence.Task;
import com.mastertech.controletarefas.service.TaskService;
import com.mastertech.controletarefas.web.dto.TaskPayload;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.util.List;

public class TaskControllerTest {
    private TaskController taskController;
    private TaskService taskService;

    @BeforeEach
    public void prepare(){
        taskService = Mockito.mock(TaskService.class);
        taskController = new TaskController(taskService);
    }

    @Test
    public void shouldListAllProjectTasks(){
        Task task = new Task();
        task.setId(1);
        task.setName("Criar post");
        task.setStatus("To Do");

        Mockito.when(taskService.getAllByProject(1)).thenReturn(List.of(task));

        List<TaskPayload> payloadList = taskController.getAllByProject(1);

        Assertions.assertEquals(1, payloadList.size());
        Assertions.assertEquals(task.getId(), payloadList.get(0).getId());
    }

    @Test
    public void shouldCreateATask(){
        int receivedProjectId;

        TaskPayload payload = new TaskPayload();
        payload.setId(1);
        payload.setName("Criar post");
        payload.setStatus("To Do");

        Task taskFromPayload = payload.buildEntity();

        Task anyTask = Mockito.any(Task.class);
        Mockito.when(taskService.create(anyTask)).thenReturn(taskFromPayload);

        TaskPayload createdTask = taskController.create(1, payload);

        Assertions.assertEquals(createdTask.getId(), payload.getId());
        Assertions.assertEquals(createdTask.getName(), payload.getName());
        Assertions.assertEquals(createdTask.getStatus(), payload.getStatus());
    }
}
