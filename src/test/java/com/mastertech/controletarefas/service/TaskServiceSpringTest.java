package com.mastertech.controletarefas.service;

import com.mastertech.controletarefas.persistence.Project;
import com.mastertech.controletarefas.persistence.Task;
import com.mastertech.controletarefas.persistence.TaskRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.util.List;

/**
 * Essa classe carrega o contexto do Spring para execução dos testes
 */
@SpringBootTest
public class TaskServiceSpringTest {
    @MockBean
    private TaskRepository taskRepository;
    @Autowired
    private TaskService taskService;

    @Test
    public void shouldListAllProjectTasks(){
        Project project = new Project();
        project.setId(1);
        project.setName("Blog");

        Task task = new Task();
        task.setId(1);
        task.setName("Criar novo post");
        task.setProject(project);

        Mockito.when(taskRepository.findAllByProjectId(1)).thenReturn(List.of(task));

        List<Task> foundTasks = taskService.getAllByProject(1);

        Assertions.assertEquals(1, foundTasks.size());
        Assertions.assertEquals(1, task.getId());
    }

    @Test
    public void shouldCreateATask(){
        Project project = new Project();
        project.setId(1);
        project.setName("Blog");

        Task task = new Task();
        task.setId(1);
        task.setName("Criar novo post");
        task.setProject(project);

        Mockito.when(taskRepository.save(task)).thenReturn(task);

        Task createdTask = taskService.create(task);

        Assertions.assertEquals(task, createdTask);
    }
}
